# Customized php-fpm Docker Image for Pimcore with phpunit

## Notes

* accessable in docker hub as *basilicom/phpunit:phpunit-7.5-php-7.4*
* includes php zip extension

## Push new version
```
$ docker build -t basilicom/phpunit:phpunit-7.5-php-7.4 .
$ docker push basilicom/phpunit:phpunit-7.5-php-7.4
```
