# PHPUnit Docker Container.
# inspired by Nicolas Frey [https://raw.githubusercontent.com/JulienBreux/phpunit-docker/master/7.4.0/dockerfile]
# how-to:
# 1. docker build -t dav-index-phpunit75 .
# 2. docker tag dav-index-phpunit75 <YOUR_HUB_HANDLE>/phpunit:7.5
# 3. docker push <YOUR_HUB_HANDLE>/phpunit:7.5
# currently accessable in docker hub as basilicom/phpunit:phpunit-7.5-php-7.4

FROM php:7.4.3-cli

ENV DEBIAN_FRONTEND noninteractive
ENV XDEBUG_MODE coverage

WORKDIR /tmp

RUN apt update -yqq && apt install -yqq \
        apt-utils \
        bash \
        ca-certificates \
        libcurl4-openssl-dev \
        libxml2-dev \
        libonig-dev \
        git \
        libzip-dev zip unzip \
        libc6 \
        wget

# Enable X-Debug
RUN pecl install xdebug && docker-php-ext-enable xdebug && php -m | grep -i xdebug

# Enable zip
RUN docker-php-ext-install zip

# Enable intl
RUN docker-php-ext-install intl

# Install redis
RUN pecl install -o -f redis \
&&  rm -rf /tmp/pear \
&&  docker-php-ext-enable redis

# Install composer + phpunit
# TODO: Update for composer 2.0
RUN wget -O composer.phar https://getcomposer.org/composer-1.phar && chmod a+x composer.phar && mv composer.phar /usr/bin/composer \
    && composer require "phpunit/phpunit:^7.5" --prefer-source --no-interaction \
    && composer require "phpunit/php-invoker" --prefer-source --no-interaction \
    && ln -s /tmp/vendor/bin/phpunit /usr/local/bin/phpunit

VOLUME ["/app"]
WORKDIR /app

ENTRYPOINT ["/usr/local/bin/phpunit"]
CMD ["--help"]
